//use clap::Parser;
//
//
//#[derive(Parser, Debug)]
//#[command(number = 0)]
//struct Args {
//    /// number to test our program
//    number: String,
//
////    count: u8,
//}
//
//
//fn main() {
//    let args = Args::parse();
//
// //   for _ in 0..args.count {
//    println!("the number is {}", args.number);
//  //  }
//}
//

use clap::Parser;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// number to test our conjecture
    number: u32,

}

fn main() {
    let args = Args::parse();

    println!("Hello {}!", args.number)
}
