use std::io;
use std::io::Write;
use clap::Parser;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// number to test our conjecture
    number: u32,

}


fn check_largest(mut largest_number: u64, number: u64) -> u64
{
    if number > largest_number {
        largest_number = number
    }
    largest_number
}

fn is_prime(n: u64) -> bool {
    if n <= 1 {
        return false;
    }
    for a in 2..n {
        if n % a == 0 {
            return false; // if it is not the last statement you need to use `return`
        }
    }
    true // last value to return
}

fn main() {
    let mut steps = 0;
    let mut largest_number: u64 = 0;
    print!("enter a number: ");
    let _ = io::stdout().flush();
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).expect("failed to get number");

    let mut number: u64 = input_line.trim().parse().expect("input not an integer");
    let mut list_of_numbers: Vec<u64> = Vec::new();
    let mut list_of_primes: Vec<u64> = Vec::new();

    while number != 1 {
        steps += 1;
        list_of_numbers.push(number);
        largest_number = check_largest(largest_number, number);
        if is_prime(number) {
            list_of_primes.push(number);
        }
        if number % 2 == 0 {
            number /= 2;
        }
        else {
            number *= 3 ;
            number += 1;
        }

    } 
    println!("the largest number was {largest_number}");
    println!("It took {steps} amount of steps to get there");
    println!("{:?}",list_of_primes);
}
